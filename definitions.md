# Logic of Mark Down
Text areas that are indented 
> such as this line
are considered suggestions that are subject to discussions at an early stage or provide background information.

Lines that are not indented have passed discussions and reached agreement by the frequent participants of the groups. 

# Proposed updates to existing definitions.

## Data Space

### Existing Gaia-X definition ###
See: <https://gaia-x.gitlab.io/technical-committee/glossary/data_space/>

A Data Space is a virtual data integration concept defined as a set of
participants and a set of relationships among them, where participants
provide their data resources and computing services. 

Data Spaces have the following design principles:

1.  data resides in its sources;

2.  only semantic integration of data and no common data schema;

3.  nesting and overlaps are possible;

4.  spontaneous networking of data, data visiting and coexistence of data are enabled. 

Within one Data Ecosystem, several Data Spaces can emerge.

### Proposed new Gaia-X defintion ###

**Data Space:** a self-sovereign ecosystem purposed for *data sharing*
~~the sharing of *data assets* ~~, based on a set of
 pro-competitive rules, terms and conditions governed by participants
and operated in compliance with applicable privacy, competition and
regulatory laws and provisions.

Note 1: The participants of the ecosystem define the  rules and
governance framework that suits their purpose.  

Note 2: Data may reside in multiple sources

Note 3: rules may directly derive from EU/EEA Law.

Note 4: Rules shall be transparently accessible for participants

# Proposed New definitions for consideration: 


**Data Sharing:** an unbounded set of one-time or recurring data sharing event(s) where
data resources are exchanged between participants in a data space.

> \[Background discussion : this is a necessary definition. However, the
> term should not become a duplication of the technical 'sharing'
> definition discussed in other groups (others are more looking at the
> how the sharing happens rather than IF the sharing can happen WHAT is
> this sharing about). Agreement: This new term shall be intended to
> home the legal dimension of the sharing (permitted vs not permitted in
> combination with other attributes which may render the exchange
> 'feasible' or not). \]

**Data sharing event**: a transfer of data resources occurring between
two or more participants at a given time and place, characterized by a
set of ex-ante verifiable attributes.

Note 1: data resources can be structured or unstructured.

Note 2: a transfer implementation can be push or pull, point-to-point or
point-to-multipoint.

Note 3: Minimum attributes to validate any data sharing event are
(proposed list):

> Participants IDs
>
> Data high level categorization/type (eg personal vs non personal which
> will trigger or not the 'user consent notarization)
>
> Processing purpose (we need around 5-8 categories to cover 99% use
> cases)
>
> Pass- through rights permission degree (1, 2nd or 3rd)
>
> Time interval
>
> Geographic scope
>
> Applicable limitations (eg territorial (EU only) or purpose
> exclusivity (academia but not companies).
>
> Special requirements ( eg applying after sharing event occurred on
> supplier request eg. Post use resources deletion);
>
> Compensation Y/N (vs altruism)
>
> Payment method (if compensation Y)
>
> Each of the 8 parameters would need to be 'coded' in order to be
> understandable, automatable and manageable in the federated Gaia-X
> environment
>
> Also 'data type' 'Processing purpose" and "Pass through rights
> permission" would need to be defined as well.
>
> **Data subject**
>
> \[Background discussion: Isa made a point that we completely lack the
> reference to 'data subject' in Gaiax but since EU Data Sovereignty
> Policy explicitly opens for end-users to donate or share data, how do
> we exclude them and also how do we ensure that if people donate or
> give additional sharing data processing permission to a Gaiax-
> participant, this 'pass through rights' is 'handled' appropriately in
> the GaiaX framework? Such upstream 'permission' should be readable and
> traceable all along the sharing chain. Agreement: we retain it but we
> need to discuss it more\]
>
>  
>
> **Data processing purpose**
>
> \[Background discussion: This was also identified as key element to
> any data sharing event and which we need to have added to the
> Glossary. Whichever the type or domain data/metadata at stake, sharing
> implies that a given processing purpose has been granted and this must
> be known ex ante, subject to changes over time, including possible new
> restrictions in the future. So the 'data' subject  (who really
> controls data eg a consumer for personal data but an industry in pure
> M2M cases, can 'rely' on ex ante pre-defined processing purposes that
> each party to the federation 'adhere to'. (this implies we will have
> to categorize the purposes at least in some macro categories and we
> will see next time). Agreement: we retain the term and need to work on
> it.\]
>
>
>
> **Data Type**
>
> \[Background discussion: Group is aware that Data Catalogue, Data
> Exchange etc are defined technically at Tech WG levels. Data
> interoperability as well will be guaranteed between volunteering
> parties and specified. But how Data Inventories are set up/published
> and how Data categorization is labelled is still missing in gaiaX.
> Agreement: group to continue discussion next time to see if we can be
> the work specifying it or delegating another group to do it.\]

## Other terms we may want to define if dictionary definitions are not suitable. 

> **Voluntarily**
>
> Background discussion: Some discussion went on the concept of
> voluntarily 'sharing'. Group agrees that the notion of voluntarily is
> important (by default), as the whole Gaiax framework is voluntarily
> based but Isa thinks that when thinking of Public Administration data
> (outside the security scope) and the fact that Open Data directive
> applies, and the fact that public administration can also be a 'user'
> of GaiaX, the notion of volunteering 'data' to share rather be 'always
> voluntarily except whe 'public laws an regulation' provisions state
> differently so that private industry will not be mandated to 'share'
> anything except for those categories for which the 'mandate already
> exists (Banks) and the whole Public Administration will fall in
> (eventually if EU decides mandatory sharing for gatekeepers, it will
> also fall in).
>
>  
>
> **Reciprocity**
>
> Background discussion: Some discussion went on reciprocity and if this
> should be a mandatory requirement for parties to the sharing. The
> group opinion is that it should not be mandate but it should be
> incentivized, as Gaiax is meant to amplify the sharing and not
> restrict it even though there could be justified cases for restricting
> (eg trade secrets or IP). Isa commented that what we need to avoid is
> to open the door to the Commission to step in and regulate a  wider
> sharing obligation if they see that industry is not sufficiently 'open
> to reciprocal sharing' .
>
>  
>
> **Transparency**
>
> Background discussion: This is a clear expectation from the group that
> transparency is added as a requirement to sharing and kept as strong
> value attribute. Its implementation should means that parties to
> Gaia-X could always change/modify/reduce their data available 'for
> share' (based on the catalogue) anytime, as well as its data pricing
> anytime, or scope of sharing and processing purposes anytime, as well
> as any other important element of their offer but each and every time
> they wish to do so, it will have to  be done with sufficient
> communication transparency (and sufficient advance) for the other
> parties (using their data) to adjust/react/adapt. This requirement
> helps creating a stable environment for business cases. Agreement is
> to have the term be added to Glossary. Group to propose a definition.
